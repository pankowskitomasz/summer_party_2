import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { HomeS1Component } from './home-s1/home-s1.component';
import { HomeS2Component } from './home-s2/home-s2.component';
import { HomeS3Component } from './home-s3/home-s3.component';


@NgModule({
  declarations: [
    HomeComponent,
    HomeS1Component,
    HomeS2Component,
    HomeS3Component
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
